// app/routes.js
module.exports = function(app) {
	var models = require('./models');
	var reports = require('./reports');
	var path = require('path');
	var fs = require('fs');
	var request = require('request');
	var ei = require('easyimage');
	var mongoose = require('mongoose');
	var Q = require('q');

	var server = 'http://gac4.awebde.ch:8080/';

	var getCover = function(game) {
		var deferred = Q.defer();

		var source = game.coverArt;
		//var cover = path.join('covers', path.basename(game.coverArt).replace(/%[0-9a-zA-Z][0-9a-zA-Z]/g, ""));
		//var coverThumb = path.join('covers', 'th_' + path.basename(game.coverArt).replace(/%[0-9a-zA-Z][0-9a-zA-Z]/g, ""));
		var cover = path.join('covers', (game.title + ' - ' + game.system).replace(/[:\?!,\.\/\\]/g, "") + path.extname(game.coverArt).slice(0, 4));
		var coverThumb = path.join('covers', 'th_' + (game.title + ' - ' + game.system).replace(/[\:\?!,\.\/\\]/g, "") + path.extname(game.coverArt).slice(0, 4));

		game.coverArt = cover;
		game.coverArtThumb = coverThumb;

		var coverLocal = path.join('public', cover);
		var coverThumbLocal = path.join('public', coverThumb);
		var file = fs.createWriteStream(coverLocal);

		console.log('downloading cover from ' + game.coverArt);
		request(source).pipe(file);
		file.on('finish', function() {
			ei.resize({
				src: coverLocal,
				dst: coverThumbLocal,
				width: 110,
				height: 156
			})
			.then(function(){
				return ei.resize({
					src: coverLocal,
					dst: coverLocal,
					width: 330,
					height: 156*3
				})
			})
			.then(function(){
				deferred.resolve(true);
			});
			file.close();
		});
		return deferred.promise;
	}

	// server routes ===========================================================
	app.get('/api/reports/by-system', function(req, res) {
		reports.by('system').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/genre/:g/systems', function(req, res) {
		reports.inBy('genre', req.params.g, 'system').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/by-genre', function(req, res) {
		reports.by('genre').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/system/:s/genres', function(req, res) {
		reports.inBy('system', req.params.s, 'genre').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/by-year', function(req, res) {
		reports.by('year').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/top-devs', function(req, res) {
		reports.top(10, 'developer').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/top-devs/system/:s', function(req, res) {
		reports.topIn(10, 'system', req.params.s, 'developer').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/top-devs/genre/:g', function(req, res) {
		reports.topIn(10, 'genre', req.params.g, 'developer').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/top-pubs', function(req, res) {
		reports.top(10, 'publisher').then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/top-rated', function(req, res) {
		reports.topRated(10).then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/top-rated/system/:s', function(req, res) {
		reports.topRatedIn(10, 'system', req.params.s).then(function(data) {
			res.json(data);
		});
	});

	app.get('/api/reports/top-rated/genre/:g', function(req, res) {
		reports.topRatedIn(10, 'genre', req.params.g).then(function(data) {
			res.json(data);
		});
	});

	app.post('/api/games', function(req, res) {
		var newGame = req.body;

		console.log('inserting ' + JSON.stringify(newGame));
		newGame.insertion = new Date();
		if(newGame.coverArt) {
			getCover(newGame).then(function() {
				models.Game.create(newGame,	function(err, c) {
					if(err) res.send(err);
					else res.json(c);
				});
			});
		} else {
			models.Game.create(newGame,	function(err, c) {
				if(err) res.send(err);
				else res.json(c);
			});
		}
	});

	app.get('/api/games/count', function(req, res) {
		models.Game.count(function(err, c) {
			if(err) res.send(err);
			else res.json(c);
		});
	});

	app.get('/api/games/count/:field/:key', function(req, res) {
		models.Game.count(JSON.parse('{"'+req.params.field+'":"'+req.params.key+'"}'), function(err, c) {
			if(err) res.send(err);
			else res.json(c);
		});
	});

	app.get('/api/coverServer', function(req, res) {
		res.json(server);
	});
	
	app.get('/api/games', function(req, res) {
		console.log('querying games');
		models.Game.find().sort('-insertion').exec(function(err, games) {
			if(err) res.send(err);
			else res.json(games);
		});
	});

	app.put('/api/games/:id', function(req, res) {
		delete req.body._id;
		delete req.body.insertion;
		console.log('updating game ' + req.params.id + ' to ' + JSON.stringify(req.body));
		if(req.body.coverArt && req.body.coverArt.slice(0, 4) == 'http')
			getCover(req.body);
		models.Game.update({ _id: new mongoose.Types.ObjectId(req.params.id) }, { $set : req.body }, function(err) { 
			err && console.log(err) 
		});
	});

	app.delete('/api/games/:id', function(req, res) {
		models.Game.remove({ _id: new mongoose.Types.ObjectId(req.params.id) }, function(err) { 
			err && console.log(err) 
		});
	});

	app.get('/api/distinct/:field', function(req, res) {
		models.Game.distinct(req.params.field, JSON.parse('{"'+req.params.field+'" : { "$ne" : null } }'), function(err, keys) {
			if(err) res.send(err);
			else {
				res.json(keys.sort());
			}
		});
	});

	app.use(function(req, res) {
		res.status(404);

		// respond with html page
		if (req.accepts('html')) {
			res.render('404', { url: req.url });
			return;
		}

		// respond with json
		if (req.accepts('json')) {
			res.send({ error: 'Not found' });
			return;
		}

		// default to plain-text. send()
		res.type('txt').send('Not found');
	});
};
