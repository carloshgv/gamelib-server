var mongoose = require("mongoose");
var Schema = mongoose.Schema;

module.exports.Game = mongoose.model('Game', {
	title : String,
	developer : String,
	publisher : String,
	system : String,
	year : Number,
	genre : String,
	comments : String,
	coverArt : String,
	coverArtThumb : String,
	rating : Number,
	metascore : Number,
	insertion : Date,
	digital : Boolean,
	completion : String
});
