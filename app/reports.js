var mongoose = require('mongoose');
var Q = require('q');
var models = require('./models');

module.exports.by = function(field) {
	var deferred = Q.defer();
	models.Game.aggregate([ { $group: { _id:'$'+field, count: { $sum:1 } } }, { $sort: { _id:1 } } ])
	.exec(function(err, data) {
		if(!err) {
			var output = [];
			data.forEach(function(e) {
				output.push([ e._id, e.count ]);
			});
			deferred.resolve([ output ]);
		}
		else
			deferred.reject(err);
	});
	return deferred.promise;
};

module.exports.inBy = function(fieldA, valueA, fieldB) {
	var deferred = Q.defer();
	models.Game.aggregate([
		{ $match : JSON.parse('{"'+fieldA+'":"'+valueA+'"}') },
		{ $group : { _id : '$'+fieldB, count : { $sum:1 } } },
		{ $sort : { _id:1 } }
	]).exec(function(err, data) {
		if(!err) {
			var output = [];
			data.forEach(function(e) {
				output.push([ e._id, e.count ]);
			});
			deferred.resolve([ output ]);
		}
		else {
			console.log('error ' + err);
			deferred.reject(err);
		}
	});
	return deferred.promise;
}

module.exports.top = function(n, field) {
	var deferred = Q.defer();
	models.Game.aggregate([ { $group: { _id:'$'+field, count: { $sum:1 } } }, { $sort: { count:-1 } }, { $limit : n } ])
	.exec(function(err, data) {
		if(!err) {
			deferred.resolve(data);
		}
		else
			deferred.reject(err);
	});
	return deferred.promise;	
}

module.exports.topIn = function(n, fieldA, valueA, field) {
	var deferred = Q.defer();
	models.Game.aggregate([
		{ $match : JSON.parse('{"'+fieldA+'":"'+valueA+'"}') },
		{ $group : { _id : '$'+field, count : { $sum:1 } } }, 
		{ $sort : { count : -1 } }, 
		{ $limit : n }
	]).exec(function(err, data) {
		if(!err) {
			deferred.resolve(data);
		}
		else
			deferred.reject(err);
	});
	return deferred.promise;	
}

module.exports.topRated = function(n) {
	var deferred = Q.defer();
	models.Game.aggregate([ { $match : { rating : { $gt:0 } } }, { $sort: { rating:-1 } }, { $limit : n }, { $project : { title:1, rating:1 } } ])
	.exec(function(err, data) {
		if(!err) {
			deferred.resolve(data);
		}
		else
			deferred.reject(err);
	});

	return deferred.promise;	
}

module.exports.topRatedIn = function(n, fieldA, valueA) {
	var deferred = Q.defer();
	models.Game.aggregate([
		{ $match : JSON.parse('{"'+fieldA+'":"'+valueA+'", "rating" : { "$gt":0 } }') },
		{ $sort : { rating : -1 } }, 
		{ $limit : n },
		{ $project : { title:1, rating:1 } }
	]).exec(function(err, data) {
		if(!err) {
			deferred.resolve(data);
		}
		else
			deferred.reject(err);
	});
	return deferred.promise;	
}
